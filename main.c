#include <stdio.h>
#include <string.h>
#define NUM_VEHICULOS  10
#define NUM_OPC_MENU   3
typedef int tipo;
#define automovil 1
#define camion 2
struct vehiculo
{
	int aid;
	tipo atipo;
	char aplaca[6];
}vehiculos[NUM_VEHICULOS],aux;
int cont_vehiculos = 0;

int get_pos_vehiculo_con_placa(char[]);
int get_pos_vehiculo_con_id(int);
void add_vehiculo(int,tipo , char[]);
void  pedir_vehiculo(int *id, int *vtipo, char *placa);
int get_pos_vehiculo_con_placa(char placa[]){
	int i= 0;
	for(i=0;i<cont_vehiculos;i=i+1){
		if(strcmp(vehiculos[i].aplaca,placa)==0){
			return i;
		}
	}
	return -1;
}
int get_pos_vehiculo_con_id(int id){
	int i= 0;
	for(i=0;i<cont_vehiculos;i=i+1){
		if(vehiculos[i].aid ==id){
			return i;
		}
	}
	return -1;
}
void add_vehiculo(int id , tipo vtipo, char placa[]){
	if(get_pos_vehiculo_con_placa(placa)!= -1 ){
		printf("\nERROR: el vehiculo con la placa '%s' ya existe\n",placa);
		return;
	}
	if(get_pos_vehiculo_con_id(id) != -1){
		printf("\nERROR: el vehiculo con el id '%d' ya existe\n",id);
		return;
	}
	vehiculos[cont_vehiculos].aid =id;
	vehiculos[cont_vehiculos].atipo = vtipo;        
	strcpy(vehiculos[cont_vehiculos].aplaca, placa);
	cont_vehiculos = cont_vehiculos +1;
	printf("Exito, se anadio el vehiculo\n");
}
void eliminar_vehiculo(int id){
	if(id<1){
		printf("\nERROR: el id es invalido");
	}
	int posicion = get_pos_vehiculo_con_id(id);
	if(posicion ==-1){
		printf("\nERROR: el vehiculo no existe");
	}
	int i;
	for(i= posicion;i<cont_vehiculos-1;i=i+1){
		vehiculos[i] = vehiculos[i+1];
	}
	cont_vehiculos = cont_vehiculos-1;
}
void imprimir_vehiculo(struct vehiculo *p){
	printf ("\nid = %d",(*p).aid);
	if((*p).atipo== automovil){
		printf("\ntipo = automovil");
	}else{
		printf("\ntipo = camion");
	}
	
	printf ("\nplaca = %s\n",(*p).aplaca);
}	
void imprimir_vehiculos(void){
	struct vehiculo *auxp= vehiculos;
	int i;
	for(i=0;i<cont_vehiculos;i=i+1){
		imprimir_vehiculo(auxp);
		auxp = auxp+1;
		printf("------------------");
	}
	printf("\n");
}
void imprimir_vehiculos_del_tipo(tipo vtipo){
	struct vehiculo *auxp;
	int i;
	for(i=0;i<cont_vehiculos;i=i+1){
		auxp = vehiculos+i;
		if((*auxp).atipo == vtipo){
			imprimir_vehiculo(auxp);
			printf("------------------");
		}
		
	}
	printf("\n");
}
void pedir_vehiculos(void){
	printf("A continuacion se le pediran maximo %d vehiculos, si no quiere ingresar mas vehiculos ingrese el id 0\n",NUM_VEHICULOS);
	int id=0;
	tipo vtipo=0;
	char placa[6];
	while(cont_vehiculos < NUM_VEHICULOS){
		pedir_vehiculo(&id,&vtipo,placa);
		if(id  == 0 ){
			return;
		}else{
			add_vehiculo(id,vtipo,placa);
			imprimir_vehiculos();
		}
	}
}
tipo pedir_tipo_vehiculo(){
	int vtipo;
	do{
		printf("eliga el tipo:\n1. automovil\n2.camion\n");
		scanf("%d",&vtipo);
	}while(vtipo != 1 && vtipo != 2);
	return vtipo;
}
int pedir_id_vehiculo(){
	int id;
	do{
		printf("ingrese id, este no puede ser negativo:");
		scanf("%d",&id);
		
	}while(id <0);
	return id;
}
void pedir_placa_vehiculo(char placa[]){
	printf("ingrese la placa:");
	scanf("%s",placa);
}
void  pedir_vehiculo(int *id, int *vtipo, char *placa){
	*id = pedir_id_vehiculo();
	if(*id == 0){
		imprimir_vehiculos();
		imprimir_vehiculos();
		return;
	}
	*vtipo = pedir_tipo_vehiculo();
	pedir_placa_vehiculo(placa);
	fflush(stdin);
}

void imprimir_menu(void){
	printf("\neliga una opcion:\n");
	printf("1.Imprimir vehiculos\n");
	printf("2.Imprimir vehiculos del tipo\n");
	printf("3.Eliminar un vehiculo]\n");
	printf("%d. Salir",NUM_OPC_MENU);
}

void procesar_opcion(int opc){
	switch(opc){
		case 1:
			imprimir_vehiculos();
			break;
		case 2:
			imprimir_vehiculos_del_tipo(pedir_tipo_vehiculo());
			break;
		case 3:
			eliminar_vehiculo(pedir_id_vehiculo());
			break;
		case 4:
			printf("adios");
			break;
		default:
			printf("opc erronea");
	}
}
void iterar_menu(){
	int opc;
	do{
		imprimir_menu();
		scanf("%d",&opc);
		procesar_opcion(opc);
	}while(opc!=NUM_OPC_MENU+1);
}
int main (void)
{
	pedir_vehiculos();
	iterar_menu();
	return 0;
}
